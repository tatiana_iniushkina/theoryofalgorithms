package lesson.ita.sort;

import java.util.Arrays;

/**
 * Реализация пузырьковой сортировки в массиве целых чисел
 *
 * @author T. Iniuskina
 */
public class BubbleSort {
    public static void main(String[] args) {
        int[] array = {5, 24, 36, 8, 49, 12, 19, 20, 2, 6};
        System.out.println(Arrays.toString(array));
        bubbleSort(array);
        System.out.println(Arrays.toString(array));
    }

    /**
     * Сортирует массив {@code array} по возрастанию значений методом "пузырька"
     *
     * @param array массив целых чисел
     */
    private static void bubbleSort(int[] array) {
        for (int out = array.length - 1; out > 1; out--) {
            for (int in = 0; in < out; in++) {
                if (array[in + 1] < array[in]) {
                    int temp = array[in];
                    array[in] = array[in + 1];
                    array[in + 1] = temp;
                }
            }

        }
    }
}
