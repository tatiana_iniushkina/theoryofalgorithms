package lesson.ita.sort;

import java.util.Arrays;

/**
 * Реализация сортировки выбором в массиве целых чисел
 *
 * @author T. Iniuskina
 */
public class SelectionSort {
    public static void main(String[] args) {
        int[] array = {5, 24, 36, 8, 49, 12, 19, 20, 2, 6};
        System.out.println(Arrays.toString(array));
        selectionSort(array);
        System.out.println(Arrays.toString(array));
    }

    /**
     * Сортирует массив {@code array} по возрастанию значений методом выбора
     *
     * @param array массив целых чисел
     */
    private static void selectionSort(int[] array) {
        for (int out = 0; out < array.length - 1; out++) {
            int indexMin = out;
            for (int in = out + 1; in < array.length; in++) {
                if (array[in] < array[indexMin]) {
                    indexMin = in;
                }
            }
            int temp = array[out];
            array[out] = array[indexMin];
            array[indexMin] = temp;
        }
    }
}
