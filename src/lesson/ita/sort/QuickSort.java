package lesson.ita.sort;

import java.util.Arrays;


public class QuickSort {
    public static void main(String[] args) {
        int[] numbers = {8, 0, 4, 7, 3, 7, 10, 12, -3};
        System.out.println("Было");
        System.out.println(Arrays.toString(numbers));

        int low = 0;
        int high = numbers.length - 1;

        quickSort(numbers, low, high);
        System.out.println("Стало");
        System.out.println(Arrays.toString(numbers));
    }

    private static void quickSort(int[] array, int low, int high) {
        if (array.length == 0)
            return;//завершить выполнение, если длина массива равна 0

        if (low >= high)
            return;//завершить выполнение, если уже нечего делить

        // выбрать опорный элемент
//        int middle = low + (high - low) / 2;
        int middle = (low + high) / 2;

        int pivot = array[middle];

        // разделить на подмассивы, которые больше и меньше опорного элемента
        int i = low, j = high;
        while (i <= j) {
            while (array[i] < pivot) {
                i++;
            }

            while (array[j] > pivot) {
                j--;
            }

            if (i <= j) {//меняем местами
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
        }
        System.out.println(Arrays.toString(array));
        // вызов рекурсии для сортировки левой и правой части
        if (low < j)
            quickSort(array, low, j);

        if (high > i)
            quickSort(array, i, high);
    }


}

