package lesson.ita.sort;

import java.util.Arrays;

/**
 * Реализация сортировки вставками в массиве целых чисел
 *
 * @author T. Iniuskina
 */
public class InsertionSort {
    public static void main(String[] args) {
        int[] array = {5, 24, 36, 8, 49, 12, 19, 20, 2, 6};
        System.out.println(Arrays.toString(array));
        insertionSort(array);
        System.out.println(Arrays.toString(array));
    }

    /**
     * Сортирует массив {@code array} по возрастанию значений методом вставки
     *
     * @param array массив целых чисел
     */
    private static void insertionSort(int[] array) {
        for (int out = 1; out < array.length; out++) {
            int temp = array[out];
            int in = out;
            while (in > 0 && array[in - 1] >= temp) {
                array[in] = array[in - 1];
                in--;
            }
            array[in] = temp;

        }
    }
}
