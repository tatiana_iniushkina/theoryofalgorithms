package lesson.ita.sequentialsearch;

import java.util.ArrayList;
import java.util.Scanner;


public class SequentialSearchArrayList {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        ArrayList<Integer> integers = new ArrayList<>();
        fillArrayList(integers);
        System.out.println(integers);
        System.out.println("Введите значение, которое надо найти:");
        int value = scanner.nextInt();
        int index = sequentialSearch(integers, value);
        if (index != -1){
            System.out.println("Индекс " + value + " равен " + index);
        }else {
            System.out.println("Значения " + value + " в списке нет");
        }
    }

    private static void fillArrayList(ArrayList<Integer> integers) {
        int amount = 1  + (int)(Math.random()*20);
        for (int i = 0; i < amount; i++) {
            integers.add(i);
        }
    }

    private static int sequentialSearch(ArrayList<Integer> integers, int value) {
        for (int i = 0; i < integers.size(); i++) {
            if (integers.get(i) == value){
                return i;
            }
        }
        return -1;
    }
}
