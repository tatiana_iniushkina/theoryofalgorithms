package lesson.ita.sequentialsearch;

/**
 * Реализация последовательного поиска в массиве целых чисел
 *
 * @author T. Iniuskina
 */
public class SequentialSearch {
    public static void main(String[] args) {
        int[] array = {-5, 2, 6, 8, 12, 19, 20, 24, 36, 49};
        System.out.println(sequentialSearch(array, 8));
        System.out.println(sequentialSearch(array, 7));
    }

    /**
     * Возвращает индекс элемента, если число {@code number} найдено в массиве {@code array}
     * или -1, если искомого числа в массиве нет
     *
     * @param array упорядоченный массив цклых чисел
     * @param number искомое целое число
     * @return индекс элемента массива, если число найдено или -1 в обратном случае
     */
    private static int sequentialSearch(int[] array, int number) {
        for (int i = 0; i < array.length ; i++) {
            if (array[i] == number){
                return i;
            }
        }
        return -1;
    }
}
