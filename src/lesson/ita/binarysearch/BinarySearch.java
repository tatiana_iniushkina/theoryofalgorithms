package lesson.ita.binarysearch;

/**
 * Реализация бинарного поиска в массиве целых чисел
 *
 * @author T. Iniuskina
 */
public class BinarySearch {
    public static void main(String[] args) {
        int[] array = {-5, 2, 6, 8, 12, 19, 20, 24, 36, 49};
        System.out.println(binarySearch(array, 8));
        System.out.println(binarySearch(array, 7));
    }

    /**
     * Возвращает индекс элемента, если число {@code number} найдено в массиве {@code array}
     * или -1, если искомого числа в массиве нет
     *
     * @param array упорядоченный массив цклых чисел
     * @param number искомое целое число
     * @return индекс элемента массива, если число найдено или -1 в обратном случае
     */
    private static int binarySearch(int[] array, int number) {
        int low = 0;
        int high = array.length - 1;
        while (low <= high) {
            int mid = (high + low) / 2;
            int guess = array[mid];
            if (guess == number) {
                return mid;
            }
            if (guess < number) {
                low = mid + 1;
            } else {
                high = mid - 1;
            }
        }
        return -1;
    }
}
