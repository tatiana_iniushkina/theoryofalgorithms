package lesson.ita.objectsort;

/**
 * Класс для описания персоны
 */
@SuppressWarnings("unused")
public class Person {
    private String surname;
    private int year;

    Person(String surname, int year) {
        this.surname = surname;
        this.year = year;
    }

    String getSurname() {
        return surname;
    }

    public int getYear() {
        return year;
    }

    @Override
    public String toString() {
        return "Person{" +
                "surname='" + surname + '\'' +
                ", year=" + year +
                '}';
    }
}
