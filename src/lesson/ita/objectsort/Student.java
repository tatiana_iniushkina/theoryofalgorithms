package lesson.ita.objectsort;

/**
 * Класс для описания студента
 */
@SuppressWarnings("unused")
public class Student extends Person {
    private String specialityCode;
    private int admissionYear;
    private String group;


    Student(String surname, int year, String specialityCode, int admissionYear, String group) {
        super(surname, year);
        this.specialityCode = specialityCode;
        this.admissionYear = admissionYear;
        this.group = group;
    }

    public String getSpecialityCode() {
        return specialityCode;
    }

    public void setSpecialityCode(String specialityCode) {
        this.specialityCode = specialityCode;
    }

    public int getAdmissionYear() {
        return admissionYear;
    }

    public void setAdmissionYear(int admissionYear) {
        this.admissionYear = admissionYear;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    @Override
    public String toString() {
        return "Student{" +
                super.toString() +
                "specialityCode='" + specialityCode + '\'' +
                ", admissionYear=" + admissionYear +
                ", group='" + group + '\'' +
                '}';
    }
}
