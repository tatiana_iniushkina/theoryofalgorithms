package lesson.ita.objectsort;

import java.util.*;

/**
 * Класс, демонстрирующий работу со списками студентов и преподавателей
 */
public class AlphabeticalSorting {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        List<Person> students =
                new ArrayList<>(Arrays.asList(
                        new Student("Петров", 2001, "09.02.07", 2016, "16ИТ7"),
                        new Student("Иванов", 2002, "09.02.07", 2017, "17ИТ17"),
                        new Student("Сидоров", 2000, "09.02.05", 2015, "15ИТ17"),
                        new Student("Паньков", 2002, "09.02.07", 2017, "17ИТ17")));
        List<Person> teachers =
                new ArrayList<>(Arrays.asList(
                        new Teacher("Дынькин", 1975, 16),
                        new Teacher("Арбузов", 1960, 35),
                        new Teacher("Кабачкова", 1970, 28)));

        students.add(initStudent());
        System.out.println(students);
        sortList(students);
        printList(students);

        System.out.println(teachers);
        sortList(teachers);
        printList(teachers);
    }

    /**
     * Инициализирует объект студента данными введенными из консоли
     *
     * @return оъект студента
     */
    private static Student initStudent() {
        System.out.println("Введите фамилию:");
        String surname = scanner.next();
        System.out.println("Введите год рождения:");
        int year = scanner.nextInt();
        System.out.println("Введите код специальности:");
        String specialityCode = scanner.next();
        System.out.println("Введите год поступления:");
        int admissionYear = scanner.nextInt();
        System.out.println("Введите группу:");
        String group = scanner.next();
        return new Student(surname, year, specialityCode, admissionYear, group);
    }

    /**
     * Выводит список фамилий
     *
     * @param persons список персон
     */
    private static void printList(List<Person> persons) {
        for (Person person : persons) {
            System.out.println(person.getSurname());
        }
    }

    /**
     * Сортирует список по фамилиям в алфавитном порядке
     *
     * @param persons список персон для сортировки
     */
    private static void sortList(List<Person> persons) {
        for (int out = persons.size() - 1; out > 0; out--) {
            for (int in = 0; in < out; in++) {
                if (persons.get(in + 1).getSurname().compareTo(persons.get(in).getSurname()) < 0) {
                    Person temp = persons.get(in);
                    persons.set(in, persons.get(in + 1));
                    persons.set(in + 1, temp);
                }
            }
        }
    }
}
