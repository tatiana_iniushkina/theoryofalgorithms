package lesson.ita.objectsort;

/**
 * Класс для описания преподавателя
 */
@SuppressWarnings("unused")
public class Teacher extends Person {
    private int workExperience;

    Teacher(String surname, int year, int workExperience) {
        super(surname, year);
        this.workExperience = workExperience;
    }

    public int getWorkExperience() {
        return workExperience;
    }

    public void setWorkExperience(int workExperience) {
        this.workExperience = workExperience;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                super.toString() +
                "workExperience=" + workExperience +
                '}';
    }
}
