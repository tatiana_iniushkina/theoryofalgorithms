package lesson.ita.fibonacci;

/**
 * Реализация рекурсивного поиска n-ого по счету числа Фибоначчи
 * <p>
 * Числа Фибоначчи - это последовательность натуральных чисел,
 * которая начинается с чисел один и один,
 * а каждое следующее число равно сумме двух предыдущих:
 * F_1 = 1
 * F_2 = 1
 * F_n = F_{n - 1} + F_{n - 2}
 * Первые 10 чисел Фибоначчи:
 * 1, 1, 2, 3, 5, 8, 13, 21, 34, 55
 *
 * @author T. Iniuskina
 */
public class FibonacciRecursion {
    public static void main(String[] args) {
        int n = 7;
        System.out.println(n + "-ое значение Фибоначчи равно " + fibonacci(n));
    }

    /**
     * Возвращает значение n-ого по счету числа Фибоначчи
     *
     * @param n порядковый номер числа Фибоначчи
     * @return n-ое значение числа Фибоначчи
     */
    private static int fibonacci(int n) {
        if ((n == 1) || (n == 2)) {
            return 1;
        }
        return fibonacci(n - 2) + fibonacci(n - 1);
    }
}
