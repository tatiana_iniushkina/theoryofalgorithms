package lesson.ita.fibonacci;

/**
 * Реализация итеративного поиска n-ого по счету числа Фибоначчи
 * <p>
 * Числа Фибоначчи - это последовательность натуральных чисел,
 * которая начинается с чисел один и один,
 * а каждое следующее число равно сумме двух предыдущих:
 * F_1 = 1
 * F_2 = 1
 * F_n = F_{n - 1} + F_{n - 2}
 * Первые 10 чисел Фибоначчи:
 * 1, 1, 2, 3, 5, 8, 13, 21, 34, 55
 *
 * @author T. Iniuskina
 */
public class FibonacciIter {
    public static void main(String[] args) {
        int n = 7;
        System.out.println(n + "-ое число Фибоначчи = " + fibonacci(n));
    }

    /**
     * Возвращает значение n-ого по счету числа Фибоначчи
     *
     * @param n порядковый номер числа Фибоначчи, n > 0
     * @return n-ое значение числа Фибоначчи
     */
    private static int fibonacci(int n) {
        int a = 1;
        int b = 1;
        int next = 1;
        for (int i = 2; i < n; i++) {
            next = a + b;
            a = b;
            b = next;
        }
        return next;
    }
}
