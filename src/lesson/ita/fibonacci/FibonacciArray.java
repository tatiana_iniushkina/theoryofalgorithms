package lesson.ita.fibonacci;

import java.util.Arrays;
import java.util.Scanner;

public class FibonacciArray {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n =scanner.nextInt();
        int[] f = new int[n];
        f[0] = 1;
        f[1] = 1;
        for (int i = 2; i < n ; i++) {
            f[i] = f[i-2] + f[i-1];
        }
        System.out.println(Arrays.toString(f));
    }
}
